@extends('layout.master-email')

@section('body')
    <h2>Dear Sir / Madam</h2>

    <div>
        <p>Please click the link below to verify your account.</p>
        <p><a href="{{ url('account/verify/'.$confirmationCode) }}">{{ url('account/verify/'.$confirmationCode) }}</a></p>
    </div>
@stop