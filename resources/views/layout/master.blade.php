<!DOCTYPE html>
<html>
<head>
    <title>APC Resources - @yield('title')</title>

    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/shared.css')}}" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
</head>
<body>

@yield('body')

@yield('footer')

<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
{{--<script src="{{asset('bower_components/angular/angular.min.js')}}}}"></script>--}}

</body>
</html>