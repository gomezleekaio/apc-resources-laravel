@extends('layout.master')

@include('component.shared.navigation')

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-2 hidden-xs hidden-sm">
                @yield('side-navigation')
            </div>
            <div class="col-md-6">
                @yield('content')
            </div>
            <div class="col-md-3">
                @yield('side-content')
            </div>
        </div>
    </div>
@stop