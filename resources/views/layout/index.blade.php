@extends('layout.master')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="text-center header">
                <h1>APC Resources</h1>
                <p>A place where you can find the best resources for learning</p>
            </div>
        </div>
    </div>
</div>

@section('body')
    @yield('content')
@stop

