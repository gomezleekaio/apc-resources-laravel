
@if (count($topics) === 0)
    <div>
        <h4>No match for {{ $query }}.</h4>
    </div>
@elseif( count($topics) >= 1)
    @foreach($topics as $topic)
        <div>
            <p class="text-search-title">
                <strong><a href="{{ route('topic.show', ['id' => $topic->topic_id]) }}">{{ $topic->name }}</a></strong>
            </p>
            <p class="text-xs-comment">{{ str_limit($topic->description, 200) }}</p>
            <hr />
        </div>
    @endforeach
@endif