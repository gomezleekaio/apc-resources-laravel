<div class="topic">
    <div class="form-group">
        <h2><strong><span> {{ $topic->name }}</span></strong></h2>
    </div>
    <div class="form-group">
        <p class="topic-description">
            {{ $topic->description }}
        </p>
    </div>
    <div class="form-group">
        {{--<a class="btn btn-default topic-option" href="#" data-toggle="modal" data-target="#submit-resource"><span class="small">Submit Resource</span></a>--}}
        <a class="btn btn-default topic-option" href="{{ route('topic.create_resource', ['id' => $topic->topic_id]) }}"><span class="small">Submit Resource</span></a>
        @if(!$topic->pinned())
            <a class="btn btn-default topic-option" href="{{ route('pinner.pin', ['id' => $topic->topic_id]) }}">
                <span class="small">Pin Topic</span>
            </a>
        @else
            <a class="btn btn-default topic-option" href="{{ route('pinner.unpin', ['id' => $topic->topic_id]) }}">
                <span class="small">Unpin Topic</span>
            </a>
        @endif
    </div>
    <hr />
</div>


<div class="modal fade" id="submit-resource" role="dialog">
    <div class="modal-dialog">
        <form action="">

        </form>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Submit Resource</strong></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label" for="resource-title">Resource Title</label>
                    <input class="form-control" id="resource-title" placeholder="title" />
                    <p class="help-block msg-error">
                        @foreach($errors->get('resource-title') as $error)
                            {{ $error }}
                        @endforeach
                    </p>
                </div>
                <div class="form-group">
                    <label class="control-label" for="resource-url">Resource Url</label>
                    <input class="form-control" id="resource-url" placeholder="url" />
                    <p class="help-block msg-error">
                        @foreach($errors->get('resource-url') as $error)
                            {{$error}}
                        @endforeach
                    </p>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger" type="submit" data-dismiss="modal">Submit</button>
            </div>
        </div>

    </div>
</div>