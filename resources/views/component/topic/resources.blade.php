
<div class="resources">
    @foreach($resources as $resource)
        @include('component.resource.resource')
        <hr />
    @endforeach
    <ul class="pagination pagination-sm">
        {!! $resources->render() !!}
    </ul>
</div>