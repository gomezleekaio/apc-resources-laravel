<div class="topic-stats">
    <p><strong>Topic Stats</strong></p>
    <p class="topic-stat-item">{{ $topic->view_count }} views</p>
    <p class="topic-stat-item">{{ $topic->countResources() }} resources</p>
</div>