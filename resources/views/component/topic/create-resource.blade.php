
<div class="form-default">
    <form action="{{ route('resource.store', ['id' => $topic->topic_id]) }}" method="POST">
        {!! csrf_field() !!}
        <div>
            <a class="btn-back" href="{{ route('topic.show', ['id' => $topic->topic_id]) }}" title="Go Back"><i class="fa fa-chevron-circle-left fa-lg"></i></a>
        </div>
        <div class="form-group text-center">
            <h3><strong><span> {{ $topic->name }}</span></strong></h3>
        </div>
        <div class="form-group">
            <label class="control-label" for="resource-title">Resource Title</label>
            <input class="form-control" id="resource-title" name="resource-title" placeholder="title" />
            <p class="help-block msg-error">
                @foreach($errors->get('resource-title') as $error)
                    {{ $error }}
                @endforeach
            </p>
        </div>
        <div class="form-group">
            <label class="control-label" for="resource-url">Resource Url</label>
            <input class="form-control" id="resource-url" name="resource-url" placeholder="url" />
            <p class="help-block msg-error">
                @foreach($errors->get('resource-url') as $error)
                    {{$error}}
                @endforeach
            </p>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-danger" type="submit">Submit</button>
        </div>
    </form>
</div>