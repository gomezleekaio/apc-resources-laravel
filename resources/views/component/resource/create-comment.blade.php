<div class="add-comment">
    <form action="{{ route('resource.comment.store', ['id' => $resource->resource_id]) }}" method="POST">
        {!! csrf_field() !!}
        <div class="form-group">
            <textarea class="form-control text-xs text-xs-comment"
                      id="comment"
                      name="comment"
                      rows="4"
                      placeholder="enter your comment here"></textarea>
            <p class="help-block msg-error">
                @foreach($errors->get('comment') as $error)
                    {{ $error }}
                @endforeach
            </p>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">Add Comment</button>
        </div>
    </form>
</div>``
