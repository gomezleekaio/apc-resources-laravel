
<div class="comment">
    <p class="text-xs-comhead">{{ $comment->first_name }} {{ $comment->last_name }} <span>&nbsp;•&nbsp; {{ $comment->created_at->diffForHumans()  }}</span></p>
    <p class="text-xs text-xs-comment">{{ $comment->text }}</p>
</div>