<div class="resource">
    <div>
        @if(!$resource->upvoted())
            <a href="{{ route('upvoter.upvote', ['id' => $resource->resource_id]) }}" data-toggle="tooltip" title="Upvote">
                <i class="fa fa-caret-up fa-lg fa-fw"></i>
            </a>
        @else
            <a class="invisible" href="#">
                <i class="fa fa-caret-up fa-lg fa-fw"></i>
            </a>
        @endif
        <a href="{{ $resource->url }}">
            <strong class="resource-title"> {{ $resource->title }}</strong>
        </a>
    </div>
    <div class="resource-options">
        <span class="small resource-option">{{ $resource->vote_count }} points</span>
        <span class="small resource-option">by <a class="hover-underlined" href="#">{{ $resource->first_name }} {{ $resource->last_name }}</a></span>
        <span class="small resource-option">{{ $resource->created_at->diffForHumans() }}</span>
        <a class="resource-option hover-underlined" href="{{ route('resource.show', ['id' => $resource->resource_id]) }}">
            <span class="small">
                <i class="fa fa-comments fa-fw"></i>
                {{ $resource->comment_count }}
                {{ $resource->comment_count > 1 ? 'comments' : 'comment' }}
            </span>
        </a>
    </div>
</div>