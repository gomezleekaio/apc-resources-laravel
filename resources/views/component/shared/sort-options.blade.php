<div class="form-group">
    <label class="control-label">
            <span class="small">sort by:
            <span class="option-sort">
                <a href="{{ route($route, ['orderBy' => 'vote_count', 'order' => 'desc']) }}">votes</a> |
                <a href="{{ route($route, ['orderBy' => 'created_at', 'order' => 'desc']) }}">new</a>
            </span>
        </span>
    </label>
</div>