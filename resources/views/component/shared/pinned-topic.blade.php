
<div class="side-navigation">
    <div>
        <a href="#">
            <i class="fa fa-thumb-tack fa-fw"></i><strong> Pinned Topics</strong>
        </a>
    </div>

    <ul class="side-nav-list list-unstyled">
        @foreach($pinnedTopics as $topic)
            <li class="side-nav-item">
                <i class="fa fa-dot-circle-o fa-fw"></i>
                <a href="{{ route('topic.show', ['id' => $topic->topic_id, 'orderBy' => 'vote_count', 'order' => 'desc']) }}">
                    <strong> {{ $topic->name }}</strong>
                </a>
            </li>
        @endforeach
    </ul>
    <hr />
</div>