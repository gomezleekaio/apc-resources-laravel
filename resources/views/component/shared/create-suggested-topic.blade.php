
<div class="form-default">
    <form action="{{ route('suggested_topic.store') }}" method="POST">
        {!! csrf_field() !!}
        <div class="form-group text-center">
            <h3><strong><i class="fa fa-lightbulb-o fa-fw"></i>Suggest Topic</strong></h3>
        </div>
        <div class="form-group">
            <label class="control-label" for="topic-name">Topic Name</label>
            <input class="form-control" id="topic-name" name="topic-name" placeholder="name" />
            <p class="help-block msg-error">
                @foreach($errors->get('topic-name') as $error)
                    {{ $error }}
                @endforeach
            </p>
        </div>
        <div class="form-group">
            <label class="control-label" for="topic-description">Topic Description</label>
            <textarea class="form-control text-xs"
                      id="topic-description"
                      name="topic-description"
                      rows="10"
                      placeholder="description"></textarea>
            <p class="help-block msg-error">
                @foreach($errors->get('topic-description') as $error)
                    {{ $error }}
                @endforeach
            </p>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-danger" type="submit">Suggest</button>
        </div>
    </form>
</div>