<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('home.index') }}"><strong>APC Resources</strong></a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('home.index') }}"><i class="fa fa-home fa-fw"></i> Home</a></li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user fa-lg fa-fw"></i>
                        <strong> {{ session('user')->first_name }}</strong>
                        <i class="fa fa-caret-down fa-fw"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('suggested_topic.create') }}"><i class="fa fa-lightbulb-o fa-fw"></i> Suggest Topic</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-rss fa-fw"></i> Send Feedback</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('login.logout') }}"><i class="fa fa-power-off fa-fw"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form hidden-xs hidden-sm" action="{{ route('search.topic') }}" method="GET">
                <div class="form-group">
                    <input type="text" class="form-control" id="navbar-search" name="query" placeholder="Search Topic" />
                </div>
            </form>
        </div>
    </div>
</nav>