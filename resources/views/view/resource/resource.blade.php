@extends('layout.main')

@section('title')
    Resource - {{ $resource->title }}
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.resource.resource')
    @include('component.resource.create-comment')
    <hr />
    @include('component.resource.comments')
@stop