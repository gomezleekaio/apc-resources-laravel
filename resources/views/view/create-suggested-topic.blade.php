@extends('layout.main')

@section('title')
    Suggest Topic
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.shared.create-suggested-topic')
@stop