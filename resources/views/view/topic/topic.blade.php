@extends('layout.main')

@section('title')
    Topic - {{ $topic->name }}
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.topic.topic')

    <div class="form-group pull-right">
        <label class="control-label" for="select-sort">
            <span class="small">sort by:
            <span class="option-sort">
                <a href="{{ route('topic.show', ['id' => $topic->topic_id, 'orderBy' => 'vote_count', 'order' => 'desc']) }}">votes</a> |
                <a href="{{ route('topic.show', ['id' => $topic->topic_id, 'orderBy' => 'created_at', 'order' => 'desc']) }}">new</a>
            </span>
            </span>
        </label>
    </div>

    @include('component.topic.resources')
@stop

@section('side-content')
    @include('component.topic.topic-stat')
@stop