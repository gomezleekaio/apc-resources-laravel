@extends('layout.main')

@section('title')
    Topic - {{ $topic->name }}
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.topic.create-resource')
@stop

@section('side-content')
    @include('component.topic.topic-stat')
@stop