@extends('layout.main')

@section('title')
    Home
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.shared.sort-options', ['route' => 'home.index'])
    @include('component.topic.resources')
@stop