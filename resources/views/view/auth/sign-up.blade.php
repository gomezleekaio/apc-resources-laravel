@extends('layout.index')

@section('title')
    Sign Up
@stop

@section('content')
    <section class="sign-up">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-default form-sign-up">
                        <form action="{{route('sign-up.store')}}" method="POST" role="form">
                            <div>
                                <a class="btn-back" href="{{ route('login.index') }}"><i class="fa fa-chevron-left"></i></a>
                            </div>
                            {!! csrf_field() !!}
                            <div class="form-group text-center">
                                <div class="row">
                                    <h4><strong>Sign Up</strong></h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2 text-right">
                                        <label class="control-label" for="firstname">Name</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input class="form-control" id="firstname" name="firstname" type="text" value="{{old('firstname')}}" placeholder="first name" />
                                        <p class="help-block msg-error">
                                            @foreach($errors->get('firstname') as $error)
                                                {{$error}}
                                            @endforeach
                                        </p>
                                    </div>
                                    <div class="col-sm-5">
                                        <input class="form-control" name="lastname" type="text" value="{{old('lastname')}}" placeholder="last name" />
                                        <p class="help-block msg-error">
                                            @foreach($errors->get('lastname') as $error)
                                                {{$error}}
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2 text-right">
                                        <label class="control-label" for="email">Email</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="email" name="email" type="text" value="{{old('email')}}" placeholder="example@student.apc.edu.ph" />
                                        <p class="help-block msg-error">
                                            @foreach($errors->get('email') as $error)
                                                {{ $error }}
                                            @endforeach
                                            @if(Session::has('user-exist-message'))
                                                {{ session('user-exist-message') }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2 text-right">
                                        <label class="control-label" for="password">Password</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="password" name="password" type="password" value="{{ old('password') }}" placeholder="password" />
                                        <p class="help-block msg-error">
                                            @foreach($errors->get('password') as $error)
                                                {{ $error }}
                                            @endforeach
                                            @if(Session::has('password-not-match-message'))
                                                {{ session('password-not-match-message') }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2 text-right">
                                        <label class="control-label" for="confirm-password">Confirm Password</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="confirm-password" name="confirm-password" type="password" value="{{old('confirm-password')}}" placeholder="confirm password" />
                                        <p class="help-block msg-error">
                                            @foreach($errors->get('confirm-password') as $error)
                                                {{ $error }}
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-default btn-gray" type="submit">Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop