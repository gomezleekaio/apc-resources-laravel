@extends('layout.main')

@section('title')
    Home
@stop

@section('side-navigation')
    @include('component.shared.pinned-topic')
@stop

@section('content')
    @include('component.search.search-results')
@stop