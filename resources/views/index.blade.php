@extends('layout.index')

@section('title')
    Login
@stop

@section('content')
    <section class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    @if(Session::has('verification-message'))
                        <div class="form-group alert alert-success">
                            <p><i class="fa fa-check fa-lg fa-fw"></i>{{ session('verification-message') }}</p>
                        </div>
                    @elseif(Session::has('invalid-account-message'))
                        <div class="form-group alert alert-danger">
                            <p><i class="fa fa-times fa-lg fa-fw"></i>{{ session('invalid-account-message') }}</p>
                        </div>
                    @endif
                    <div class="form-default form-login">
                        <form action="{{ route('login.login') }}" method="post" role="form">
                            {!! csrf_field() !!}
                            <div class="form-group text-center">
                                <h4><strong>Login</strong></h4>
                            </div>
                            <div class="form-group">
                                <input class="form-control"
                                       id="email"
                                       type="text"
                                       name="email"
                                       value="{{old('email')}}"
                                       placeholder="Email">
                                <p class="help-block msg-error">
                                @foreach($errors->get('email') as $error)
                                    {{$error}}
                                @endforeach
                                </p>
                            </div>
                            <div class="form-group">
                                <input class="form-control"
                                       id="password"
                                       type="password"
                                       name="password"
                                       value="{{old('password')}}"
                                       placeholder="Password">
                                <p class="help-block msg-error">
                                @foreach($errors->get('password') as $error)
                                    {{$error}}
                                @endforeach
                                </p>
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-default" type="submit">Login</button>
                            </div>
                        </form>

                        <div>
                            <a href="#" class="small link-blue">Forgot Password?</a>
                            <a href="{{ route('sign-up.index') }}" class="small link-blue pull-right">Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop