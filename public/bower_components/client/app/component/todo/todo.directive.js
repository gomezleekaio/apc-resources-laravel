/**
 * Created by Lee on 16/9/2015.
 */


angular.module('todoApp')
    .directive('todo', todo);

function todo () {

    return {
        restrict: 'E',
        templateUrl: 'app/component/todo/todo.directive.html',
        controller: TodoController,
        controllerAs: 'todo'
    };
}

TodoController.$inject = ['todoService', '$window'];

function TodoController (todoService, $window) {
    var self = this;
    self.todo = '';
    self.todos = [];
    self.filter = '';

    self.init = function() {
        self.getTodos();
    };

    self.getTodos = function () {
        todoService.getTodos()
            .success(function (data) {
                self.todos = data;
            });
    };

    self.addTodo = function () {
        todoService.addTodo(self.todo)
            .success(function () {
                self.getTodos();
            });
        self.todo = '';
    };

    self.removeTodo = function (id) {
        todoService.removeTodo(id)
            .success(function () {
                self.getTodos();
            });
    };

    self.init();
}