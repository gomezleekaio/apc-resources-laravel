/**
 * Created by Lee on 16/9/2015.
 */

angular.module('todoApp')
    .factory('todoService', todoService);

todoService.$inject = ['$http'];

function todoService($http) {

    return {
        getTodos: getTodos,
        addTodo: addTodo,
        removeTodo: removeTodo
    };

    function getTodos() {
        return $http.get('api/todo/index');
    }

    function addTodo (task) {
        return $http.post('api/todo/add-todo', {task: task});
    }

    function removeTodo(id) {
        return $http.get('api/todo/destroy/'+id);
    }
}
