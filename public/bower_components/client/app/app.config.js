/**
 * Created by Lee on 16/9/2015.
 */

angular
    .module('apcResourcesApp')
    .config(config);

function config($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'app/views/login.html'
        })
        .otherwise({
            redirectTo: '/login'
        });
}