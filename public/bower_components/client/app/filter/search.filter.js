/**
 * Created by Lee on 17/9/2015.
 */

angular
    .module('todoApp')
    .filter('search', search)

function search() {

    return filter;

    function filter(input, query) {

        if (!input || !query) return input;

        var out = [];

        angular.forEach(input, function(value) {

            if (value.task.indexOf(query) > -1) {
                out.push(value);
            }
        });

        return out;
    }
}