<!DOCTYPE html>
<html>
<head>
    <title>APC Resources</title>

    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css" />
</head>
<body>
<h3>Bootstrap is installed</h3>
<i class="fa fa-lightbulb-o fa-4x"></i>

<div class="view-container">
    <div ng-view class="view-frame"></div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/angular/angular.min.js"></script>
<script src="bower_components/angular-route/angular-route.min.js"></script>


<script src="app/app.module.js"></script>
<script src="app/app.config.js"></script>

<script src="app/component/todo/todo.directive.js"></script>

<script src="app/services/todo.service.js"></script>

<script src="app/filter/search.filter.js"></script>


</body>
</html>