<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|regex:/[A-z0-9]@student.apc.edu.ph/',
            'password' => 'required|min:6',
            'confirm-password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'regex' => ':attribute must be a valid APC email address'
        ];
    }
}