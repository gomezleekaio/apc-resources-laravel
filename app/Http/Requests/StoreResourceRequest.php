<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreResourceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resource-title' => 'required|max:100',
            'resource-url' => 'required|url',
        ];
    }

    public function messages() {

        return [
            'resource-title.required' => 'title is required',
            'resource-url.required' => 'url is required',
            'resource-url.url' => 'url must be a valid url',
        ];
    }
}
