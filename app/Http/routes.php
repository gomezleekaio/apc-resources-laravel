<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

// Login
Route::get('login/index', ['as' => 'login.index', 'uses' => 'Auth\LoginController@index']);
Route::post('login', ['as' => 'login.login', 'uses' => 'Auth\LoginController@login']);
Route::get('login/logout', ['as' => 'login.logout', 'uses' => 'Auth\LoginController@logout']);

// Sign Up
Route::resource('sign-up', 'Auth\SignUpController');
Route::get('account/verify/{code}', ['as' => 'account.verify','uses' => 'Auth\SignUpController@confirmAccount']);

// Search
Route::get('search/topic', ['as' => 'search.topic', 'uses' => 'SearchController@searchTopic']);

// Home
Route::get('home/{orderBy?}/{order?}', ['as' => 'home.index', 'uses' => 'HomeController@index']);

// Topic
Route::get('topic/{id}/create-resource', ['as' => 'topic.create_resource', 'uses' => 'TopicController@createResource']);
Route::get('topic/{id}/{orderBy?}/{order?}', ['as' => 'topic.show', 'uses' => 'TopicController@showTopic']);

// Pinner
Route::get('pinner/{id}/pin', ['as' => 'pinner.pin', 'uses' => 'PinnerController@pin']);
Route::get('pinner/{id}/unpin', ['as' => 'pinner.unpin', 'uses' => 'PinnerController@unpin']);

// Resource
Route::get('resource/{id}', ['as' => 'resource.show', 'uses' => 'ResourceController@showResource']);
Route::post('resource/{id}', ['as' => 'resource.store', 'uses' => 'ResourceController@submitResource']);
Route::post('resource/{id}/comment', ['as' => 'resource.comment.store', 'uses' => 'ResourceController@addComment']);

// Upvoter
Route::get('upvoter/{id}/upvote', ['as' => 'upvoter.upvote', 'uses' => 'UpvoterController@upvote']);

// Suggested Topic
Route::get('suggested-topic/create', ['as' => 'suggested_topic.create', 'uses' => 'SuggestedTopicController@create']);
Route::post('suggested-topic', ['as' => 'suggested_topic.store', 'uses' => 'SuggestedTopicController@suggestTopic']);

/* --- Web services ---- */

// Login
Route::post('ws/login', ['uses' => 'WebServiceController@login']);

// Resource
Route::post('ws/resources', ['uses' => 'WebServiceController@getAllResources']);
Route::post('ws/resource/{id}', ['uses' => 'WebServiceController@getResources']);

// Topic
Route::post('ws/topic/{id}', ['uses' => 'WebServiceController@getTopic']);

// Pinned Topics
Route::post('ws/pinned-topics/{id}', ['uses' => 'WebServiceController@getPinnedTopics']);

Route::post('api/test', ['uses' => 'WebServiceController@testAPI']);
