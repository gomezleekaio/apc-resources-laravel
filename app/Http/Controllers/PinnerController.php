<?php

namespace App\Http\Controllers;

use App\Dao\PinnerDao;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PinnerController extends Controller
{
    private $userID;
    private $pinnerDao;

    function __construct(PinnerDao $pinnerDao) {
        $this->userID = session('user')->user_id;
        $this->pinnerDao = $pinnerDao;
    }

    public function pin($topicID) {
        if(!$this->pinnerDao->isTopicPinned($this->userID, $topicID)) {
            $this->pinnerDao->pinTopic($this->userID, $topicID);
        }
        return back();
    }

    public function unpin($topicID) {
        if($this->pinnerDao->isTopicPinned($this->userID, $topicID)) {
            $this->pinnerDao->unpinTopic($this->userID, $topicID);
        }
        return back();
    }
}
