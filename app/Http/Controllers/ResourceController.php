<?php

namespace App\Http\Controllers;

use App\Dao\CommentDao;
use App\Dao\TopicDao;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\StoreResourceRequest;
use App\Dao\ResourceDao;

class ResourceController extends Controller
{
    private $userID;
    private $resourceDao;
    private $topicDao;
    private $commentDao;

    public function __construct(ResourceDao $resourceDao, TopicDao $topicDao, CommentDao $commentDao) {
        $this->userID = session('user')->user_id;
        $this->resourceDao = $resourceDao;
        $this->topicDao = $topicDao;
        $this->commentDao = $commentDao;
    }

    public function showResource($resourceID) {
        $resource = $this->resourceDao->getResource($resourceID);
        $comments = $this->commentDao->getComments($resourceID);
        return view('view.resource.resource', compact('resource', 'comments'));
    }

    /*
     * Adds a resource to a specific topic
     *
     * @param topicID - topic id
     * */
    public function submitResource(StoreResourceRequest $request, $topicID) {
        $this->resourceDao->addResource($request->get('resource-title'), $request->get('resource-url'), $this->userID, $topicID);
        $topic = $this->topicDao->getTopic($topicID);
        return redirect()->route('topic.show', ['id' => $topic, 'orderBy' => 'vote_count', 'order' => 'desc']);
    }

    /*
     * Adds a comment to a specific resource.
     *
     * */
    public function addComment(StoreCommentRequest $request, $resourceID) {
        $this->commentDao->addComment($request->get('comment'), $this->userID, $resourceID);
        $this->resourceDao->incrementComment($resourceID);
        return redirect()->back();
    }
}