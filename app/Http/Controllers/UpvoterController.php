<?php

namespace App\Http\Controllers;

use App\Dao\ResourceDao;
use App\Dao\UpvoterDao;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UpvoterController extends Controller
{
    private $userID;
    private $upvoterDao;
    private $resourceDao;

    function __construct(UpvoterDao $upvoterDao, ResourceDao $resourceDao) {
        $this->userID = session('user')->user_id;
        $this->upvoterDao = $upvoterDao;
        $this->resourceDao = $resourceDao;
    }

    public function upvote($resourceID) {
        $this->resourceDao->incrementVote($resourceID);
        $this->upvoterDao->addUpvoter($this->userID, $resourceID);
        return back();
    }
}
