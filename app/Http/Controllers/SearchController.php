<?php

namespace App\Http\Controllers;

use App\Dao\TopicDao;
use App\Http\Requests\SearchTopicRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    private $topicDao;

    /**
     * SearchController constructor.
     *
     * @param $topicDao
     */
    public function __construct(TopicDao $topicDao)
    {
        $this->topicDao = $topicDao;
    }

    /*
     *
     * */
    public function searchTopic(Request $request) {
        $query = $request->get('query');
        $topics = $this->topicDao->searchTopic($query);
        return view('view.search', compact('topics', 'query'));
    }
}
