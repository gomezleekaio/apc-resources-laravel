<?php

namespace App\Http\Controllers;

use App\Dao\SuggestedTopicDao;
use App\Http\Requests\StoreSuggestedTopicRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SuggestedTopicController extends Controller
{

    private $suggestedTopicDao;

    /**
     * SuggestedTopicController constructor.
     * @param $suggestedTopicDao
     */
    public function __construct(SuggestedTopicDao $suggestedTopicDao)
    {
        $this->suggestedTopicDao = $suggestedTopicDao;
    }

    public function create() {
        return view('view.create-suggested-topic');
    }

    public function suggestTopic(StoreSuggestedTopicRequest $request) {
        $this->suggestedTopicDao->addSuggestedTopic($request->get('topic-name'), $request->get('topic-description'), session('user')->user_id);
        return redirect()->back();
    }
}
