<?php

namespace App\Http\Controllers\Auth;

use App\Dao\UserDao;
use App\Http\Requests\StoreUserLoginRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LoginController extends Controller
{

    private $userDao;

    /**
     * LoginController constructor.
     * @param $userDao
     */
    public function __construct(UserDao $userDao)
    {
        $this->userDao = $userDao;
    }

    public function index()
    {
        return view('index');
    }

    public function login(StoreUserLoginRequest $request)
    {
        try {
            $user = $this->userDao->authorize($request->get('email'), $request->get('password'));
            session(['user' => $user]);
            return redirect()->route('home.index');
        } catch (ModelNotFoundException $e) {
            Session::flash('invalid-account-message', 'Invalid username or password');
            return back();
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }
}