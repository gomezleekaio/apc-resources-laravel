<?php

namespace App\Http\Controllers\Auth;

use App\Dao\UserDao;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class SignUpController extends Controller
{

    private $userDao;

    /**
     * SignUpController constructor.
     * @param $userDao
     */
    public function __construct(UserDao $userDao)
    {
        $this->userDao = $userDao;
    }


    public function index() {
        return view('view.auth.sign-up');
    }

    /*
     * creates a user account and sends an email with a confirmation link.
     * */
    public function store(StoreUserRequest $request) {

        $email = $request->get('email');
        $password = $request->get('password');
        $confirmPassword = $request->get('confirm-password');
        $confirmationCode = str_random(30);

        if ($this->userDao->isUserExists($email)) {
            Session::flash('user-exist-message', 'A user with this e-mail address already exists. Try another email.');
            return redirect()->back()->withInput();
        }

        // check if password and confirm password match
        if ($password !== $confirmPassword) {
            Session::flash('password-not-match-message', 'Passwords don\'t match. Please try again ');
            return redirect()->back()->withInput();
        }

        User::create([
            'email' => $email,
            'password' => $password,
            'first_name' => $request->get('firstname'),
            'last_name' => $request->get('lastname'),
            'confirmation_code' => $confirmationCode,
        ]);

        Mail::send('email.confirmation', compact('confirmationCode'), function($message) use($email) {
            $message->from('apcResources@gmail.com');
            $message->to($email);
            $message->subject('APC Resources Email Verification');
        });

        return redirect()->route('login.index');
    }

    /*
     * checks if the confirmation code exists in the database.
     * */
    public function confirmAccount($confirmationCode) {

        if (!$confirmationCode) {
            return redirect()->route('login.index');
        }

        try {
            $user = User::where('confirmation_code', $confirmationCode)->firstOrFail();
            $user->confirmed = 1;
            $user->confirmation_code = null;
            $user->save();
            Session::flash('verification-message', 'Congratulations, '. $user->email .' has been verified. You may now login');
        } catch(ModelNotFoundException $e) {

        }

        return redirect()->route('login.index');
    }
}
