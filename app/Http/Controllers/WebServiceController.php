<?php

namespace App\Http\Controllers;

use App\Models\Pinner;
use App\Models\Resource;
use App\Models\Topic;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;



class WebServiceController extends Controller
{

    public function testAPI(Request $request) {
      $comment = Comment::create([
          'text' => $request->get('comment'),
          'user_id' => $request->get('user_id'),
          'resource_id' => $request->get('resource_id'),
      ]);
      return response()->json($comment);
    }

    /*
     * Returns the user id
     * */
    public function login(Request $request) {

        try {
            $id = User::select('user.user_id')
                ->where('email', $request->get('email'))
                ->where('password', $request->get('password'))
                ->first();
        } catch (ModelNotFoundException $e) {
            $id = null;
        }

        return response()->json($id);
    }

    /*
     * Returns all the resources of a specific topic sorted by vote count
     *
     * $id = topic id
     * */
    public function getResources($id) {
        $resources = Resource::select('topic.name','resource.resource_id',  'resource.title',
            'resource.url', 'resource.vote_count', 'resource.comment_count', 'resource.created_at', 'user.first_name', 'user.last_name')
            ->join('user', 'user.user_id', '=', 'resource.user_id')
            ->join('topic', 'topic.topic_id', '=', 'resource.topic_id')
            ->where('resource.topic_id', $id)
            ->orderBy('resource.vote_count', 'desc')
            ->get();
        return response()->json($resources);
    }

    /*
     * Returns all of the resources on the database sorted by vote count
     * */
    public function getAllResources() {
        $resources = Resource::select('topic.name','resource.resource_id',  'resource.title',
            'resource.url', 'resource.vote_count', 'resource.comment_count', 'resource.created_at', 'user.first_name', 'user.last_name')
            ->join('user', 'user.user_id', '=', 'resource.user_id')
            ->join('topic', 'topic.topic_id', '=', 'resource.topic_id')
            ->orderBy('resource.vote_count', 'desc')
            ->get();
        return response()->json($resources);
    }

    /*
     * Returns a topic
     *
     * $id - topic id
     * */
    public function getTopic($id) {
        Topic::where('topic_id', $id)->increment('view_count');
        $topic = Topic::findOrFail($id);
        $topic["resource_count"] = Resource::where('topic_id', $id)->count();
        return $topic;
    }

    /*
     * Returns the user's pinned topics.
     *
     * $id = user id
     * */
    public function getPinnedTopics($id) {
        $topics = Pinner::select('topic.topic_id', 'topic.name', 'pinner.created_at')
            ->join('user', 'user.user_id', '=', 'pinner.user_id')->where('pinner.user_id', $id)
            ->join('topic', 'topic.topic_id', '=', 'pinner.topic_id')
            ->orderBy('pinner.created_at', 'asc')
            ->get();
        return response()->json($topics);
    }
}
