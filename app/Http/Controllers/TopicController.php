<?php

namespace App\Http\Controllers;

use App\Dao\ResourceDao;
use App\Dao\TopicDao;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{
    private $topicDao;
    private $resourceDao;

    function __construct(TopicDao $topicDao, ResourceDao $resourceDao) {
        $this->topicDao = $topicDao;
        $this->resourceDao = $resourceDao;
    }

    public function showTopic($topicID, $orderBy = 'vote_count', $order = 'desc') {
        $this->topicDao->incrementView($topicID);
        $topic = $this->topicDao->getTopic($topicID);
        $resources = $this->resourceDao->getResources($topicID, $orderBy, $order);
        $resources->setPath(route('topic.show', ['id' => $topicID, 'orderBy' => $orderBy, 'order' => $order]));
        return view('view.topic.topic', compact('topic', 'resources'));
    }

    public function createResource($topicID) {
        $topic = $this->topicDao->getTopic($topicID);
        return view('view.topic.create-resource', compact('topic'));
    }
}
