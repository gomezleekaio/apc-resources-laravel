<?php

namespace App\Http\Controllers;

use App\Dao\ResourceDao;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    private $resourceDao;

    /**
     * HomeController constructor.
     *
     * @param $resourceDao
     */
    public function __construct(ResourceDao $resourceDao)
    {
        $this->resourceDao = $resourceDao;
    }

    public function index($orderBy = 'vote_count', $order = 'desc') {
        $resources = $this->resourceDao->getAllResources($orderBy, $order);
        return view('view.home', compact('resources'));
    }
}
