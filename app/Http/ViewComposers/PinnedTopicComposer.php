<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 25/9/2015
 * Time: 8:58 PM
 */

namespace App\Http\ViewComposers;


use App\Dao\PinnerDao;

class PinnedTopicComposer {

    public function compose($view) {
        $userID = session('user')->user_id;
        $pinnerDao = new PinnerDao();
        $pinnedTopics = $pinnerDao->getPinnedTopics($userID);
        $view->with(compact('pinnedTopics'));
    }
}