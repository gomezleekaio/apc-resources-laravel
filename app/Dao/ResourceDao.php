<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 26/9/2015
 * Time: 5:11 PM
 */

namespace App\Dao;

use App\Http\Requests\StoreResourceRequest;
use App\Models\Resource;

class ResourceDao {


    /*
     * Adds a resource to a specific topic
     * */
    public function addResource($title, $url, $userID, $topicID) {
        Resource::create([
            'title' => $title,
            'url' => $url,
            'user_id' => $userID,
            'topic_id' => $topicID,
        ]);
    }

    /**
     * Get a specific resource
     *
     * @param $id = resource id
     */
    public function getResource($id) {
        return Resource::select('user.first_name', 'user.last_name','resource.resource_id',  'resource.title',
            'resource.url', 'resource.vote_count', 'resource.comment_count', 'resource.created_at')
            ->join('user', 'user.user_id', '=', 'resource.user_id')
            ->where('resource.resource_id', $id)
            ->first();
    }

    /**
     * Returns all of the resources of a specific topic
     *
     * @param $id = topic id
     */
    public function getResources($id, $orderBy, $order) {
        return Resource::select('user.first_name', 'user.last_name','resource.resource_id',  'resource.title',
            'resource.url', 'resource.vote_count', 'resource.comment_count', 'resource.created_at')
            ->join('user', 'user.user_id', '=', 'resource.user_id')
            ->where('resource.topic_id', $id)
            ->orderBy($orderBy, $order)
            ->paginate(5);
    }

    /*
     * Returns all of the resources on the database sorted by vote count
     *
     * */
    public function getAllResources($orderBy, $order) {
        return Resource::select('topic.name','resource.resource_id',  'resource.title',
            'resource.url', 'resource.vote_count', 'resource.comment_count', 'resource.created_at', 'user.first_name', 'user.last_name')
            ->join('user', 'user.user_id', '=', 'resource.user_id')
            ->join('topic', 'topic.topic_id', '=', 'resource.topic_id')
            ->orderBy($orderBy, $order)
            ->paginate(10);
    }

    public function incrementVote($id) {
        Resource::where('resource_id', $id)->increment('vote_count');
    }

    public function incrementComment($id) {
        Resource::where('resource_id', $id)->increment('comment_count');
    }

    /**
     * Counts the total number of resources of a topic.
     *
     * @param $id = topic id
     */
    public function countResources($id) {
        return Resource::where('topic_id', $id)->count();
    }
}