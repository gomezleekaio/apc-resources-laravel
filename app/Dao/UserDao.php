<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 24/9/2015
 * Time: 5:21 PM
 */

namespace App\Dao;


use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserDao {

    public function authorize($email, $password) {
        return User::where('email', $email)
            ->where('password', $password)
            ->where('confirmed', 1)
            ->firstOrFail();
    }

    public function getUser($email) {
        return User::where('email', $email)
            ->firstOrFail();
    }

    public function getUsers() {
        return User::all();
    }

    public function isUserExists($email) {

        if (User::where('email', $email)->count() > 0) {
            return true;
        }
        return false;
    }
}
