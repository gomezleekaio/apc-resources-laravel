<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 1/10/2015
 * Time: 4:12 PM
 */

namespace App\Dao;

use App\Models\Upvoter;

class UpvoterDao {

    public function addUpvoter($userID, $resourceID) {
        Upvoter::create(['user_id' => $userID, 'resource_id' => $resourceID]);
    }

    public function isResourceUpvoted($userID, $resourceID) {
        $upvoter = Upvoter::where('user_id', $userID)
            ->where('resource_id', $resourceID)
            ->first();
        if ($upvoter != null) {
            return true;
        }
        return false;
    }

    /**
     * @param $id - resource id
     */
    public function countVotes($id) {
        return Upvoter::where('resource_id', $id)
            ->count();
    }
}