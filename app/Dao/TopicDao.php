<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 25/9/2015
 * Time: 1:39 PM
 */

namespace App\Dao;

use App\Models\Topic;

class TopicDao {

    /**
     * @param $id - topic id
     */
    public function getTopic($id) {
        return Topic::findOrFail($id);
    }

    /**
     * returns all the topics in the database in a form of associative array
     */
    public function getTopics() {
        return Topic::all();
    }

    public function searchTopic($query) {
        return Topic::where('name', 'LIKE', '%'.$query.'%')->get();
    }

    /**
     * @param $id - topic id
     */
    public function incrementView($id) {
        Topic::where('topic_id', $id)
            ->increment('view_count');
    }
}