<?php
/**
 * Created by PhpStorm.
 * User: gomezleekaio
 * Date: 21/1/16
 * Time: 10:00 PM
 */

namespace App\Dao;


use App\Models\SuggestedTopic;

class SuggestedTopicDao
{

    /*
     * adds a suggested topic
     *
     * @param $topicName - topic's name
     * @param $topicDescription - topic's description
     * @param $userID - user id of the user who suggested the topic
     * */
    public function addSuggestedTopic($topicName, $topicDescription, $userID) {
        SuggestedTopic::create([
            'name' => $topicName,
            'description' => $topicDescription,
            'user_id' => $userID,
        ]);
    }
}