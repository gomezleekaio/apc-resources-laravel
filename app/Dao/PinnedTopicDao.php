<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 24/9/2015
 * Time: 5:20 PM
 */

namespace App\Dao;

use App\Models\PinnedTopic;

class PinnedTopicDao {

    public function getPinnedTopics($id) {
        return PinnedTopic::select('topic.topic_id', 'topic.name')
            ->join('user', 'user.user_id', '=', 'pinner.user_id')->where('pinner.user_id', $id)
            ->join('topic', 'topic.topic_id', '=', 'pinner.topic_id')
            ->get();
    }
}