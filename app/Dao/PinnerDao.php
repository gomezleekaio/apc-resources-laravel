<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2/10/2015
 * Time: 1:23 PM
 */

namespace App\Dao;

use App\Models\Pinner;

class PinnerDao {

    /**
     * Get the topics pinned by the user from the database.
     *
     * @param $id - User's id
     * @return
     */
    public function getPinnedTopics($id) {
        return Pinner::select('topic.topic_id', 'topic.name', 'pinner.created_at')
            ->join('user', 'user.user_id', '=', 'pinner.user_id')->where('pinner.user_id', $id)
            ->join('topic', 'topic.topic_id', '=', 'pinner.topic_id')
            ->orderBy('pinner.created_at', 'asc')
            ->get();
    }

    /**
     * Pins a topic
     *
     * @param $userID - user id
     * @param $topicID - topic id
     */
    public function pinTopic($userID, $topicID) {
        Pinner::create(['user_id' => $userID, 'topic_id' => $topicID]);
    }

    /*
     * Unpins a topic of a user
     * */
    public function unpinTopic($userID, $topicID) {
        Pinner::where('user_id', $userID)
            ->where('topic_id', $topicID)
            ->delete();
    }

    /**
     * Returns true if the topic is already pinned by the user
     * otherwise, it returns false
     *
     * @param $userID - User's id
     * @param $topicID - Topic's id
     * @return bool
     */
    public function isTopicPinned($userID, $topicID) {
        $pinner = Pinner::where('user_id', $userID)
            ->where('topic_id', $topicID)
            ->first();
        if ($pinner != null) {
            return true;
        }
        return false;
    }
}