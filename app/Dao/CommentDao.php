<?php

namespace App\Dao;

use App\Models\Comment;
use App\Models\Resource;

class CommentDao {


    public function addComment($comment, $userID, $resourceID) {
        Comment::create([
            'text' => $comment,
            'user_id' => $userID,
            'resource_id' => $resourceID,
        ]);
    }

    /*
     * returns all the comments that belongs to a specific topic.
     *
     * @param $id - resource id
     * */
    public function getComments($id, $order = 'desc') {
        $comments = Comment::select('user.first_name', 'user.last_name', 'comment.text', 'comment.created_at')
            ->join('user', 'user.user_id', '=', 'comment.user_id')
            ->orderBy('comment.created_at', $order)
            ->where('comment.resource_id', $id)
            ->get();
        return $comments;
    }

}


