<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    protected $fillable = ['email', 'password', 'first_name', 'last_name', 'confirmed', 'confirmation_code'];
    protected $hidden = ['password', 'remember_token'];
}
