<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuggestedTopic extends Model
{
    protected $table = 'suggested_topic';
    protected $primaryKey = 'suggested_topic_id';
    protected $fillable = ['name', 'description', 'user_id'];
}
