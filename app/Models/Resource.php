<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Dao\UpvoterDao;

class Resource extends Model
{
    protected $table = 'resource';
    protected $primaryKey = 'resource_id';
    protected $fillable = ['title', 'url', 'user_id', 'topic_id'];

    public function upvoted() {
        $upvoterDao = new UpvoterDao();
        $userID = session('user')->user_id;
        return $upvoterDao->isResourceUpvoted($userID, $this->resource_id);
    }
}
