<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $primaryKey = 'comment_id';
    protected $fillable = ['text', 'user_id', 'resource_id', 'created_at'];
}
