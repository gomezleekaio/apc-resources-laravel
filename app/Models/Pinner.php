<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pinner extends Model
{
    protected $table = 'pinner';
    protected $primaryKey = 'pinner_id';
    protected $fillable = ['user_id', 'topic_id'];
}
