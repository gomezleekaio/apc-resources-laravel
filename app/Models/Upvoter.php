<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upvoter extends Model
{
    protected $table = 'upvoter';
    protected $primaryKey = 'upvoter_id';
    protected $fillable = ['user_id', 'resource_id'];
}
