<?php

namespace App\Models;

use App\Dao\PinnerDao;
use App\Dao\ResourceDao;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topic';
    protected $primaryKey = 'topic_id';
    protected $fillable = ['name', 'description', 'view_count'];

    public function countResources() {
        $resourceDao = new ResourceDao();
        return $resourceDao->countResources($this->topic_id);
    }

    public function pinned() {
        $pinnerDao = new PinnerDao();
        $userID = session('user')->user_id;
        return $pinnerDao->isTopicPinned($userID, $this->topic_id);
    }
}
