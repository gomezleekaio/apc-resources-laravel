<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->increments('comment_id');
            $table->mediumText('text');
            $table->integer('user_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->foreign('user_id')
                ->references('user_id')->on('user')
                ->onDelete('cascade');
            $table->foreign('resource_id')
                ->references('resource_id')->on('resource')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment');
    }
}
