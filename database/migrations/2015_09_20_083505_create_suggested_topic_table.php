<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestedTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_topic', function (Blueprint $table) {
            $table->increments('suggested_topic_id');
            $table->string('name', 50);
            $table->mediumText('description');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('user_id')->on('user')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suggested_topic');
    }
}
