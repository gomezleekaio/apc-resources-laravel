<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource', function (Blueprint $table) {
            $table->increments('resource_id');
            $table->string('title', 100);
            $table->string('url', 2083);
            $table->integer('vote_count')->default(0);
            $table->integer('comment_count')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('topic_id')->unsigned();
            $table->foreign('user_id')
                ->references('user_id')->on('user')
                ->onDelete('cascade');
            $table->foreign('topic_id')
                ->references('topic_id')->on('topic')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resource');
    }
}
