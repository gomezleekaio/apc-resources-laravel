<?php

use Illuminate\Database\Seeder;

class ResourceTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resource')->insert([

            // AngularJS
            ['vote_count' => 40, 'title' => 'AngularJS Official Documentation', 'url' => 'https://docs.angularjs.org/guide', 'user_id' => 1, 'topic_id' => 1, 'created_at' => '2015-08-05 13:02:34'],
            ['vote_count' => 32, 'title' => 'Thinkster - A better way to learn angularjs', 'url' => 'https://thinkster.io/a-better-way-to-learn-angularjs', 'user_id' => 2, 'topic_id' => 1, 'created_at' => '2015-09-06 13:02:34'],
            ['vote_count' => 25, 'title' => 'Code Academy', 'url' => 'https://www.codecademy.com/learn/learn-angularjs', 'user_id' => 3, 'topic_id' => 1, 'created_at' => '2015-10-03 13:02:34'],
            ['vote_count' => 12, 'title' => 'Egg Head', 'url' => 'https://egghead.io/articles/new-to-angularjs-start-learning-here', 'user_id' => 4, 'topic_id' => 1, 'created_at' => '2015-11-07 10:02:34'],
            ['vote_count' => 10, 'title' => 'w3schools', 'url' => 'http://www.w3schools.com/angular/', 'user_id' => 1, 'topic_id' => 1, 'created_at' => '2016-01-05 13:02:34'],

            // ReactJS
            ['vote_count' => 21, 'title' => 'ReactJS Official Documentation', 'url' => 'https://docs.angularjs.org/guide', 'user_id' => 1, 'topic_id' => 2, 'created_at' => '2015-01-05 13:02:34'],
            ['vote_count' => 16, 'title' => 'Scotch.io', 'url' => 'https://scotch.io/tutorials/learning-react-getting-started-and-concepts', 'user_id' => 2, 'topic_id' => 2, 'created_at' => '2015-07-03 13:02:34'],
            ['vote_count' => 5, 'title' => 'Build width React', 'url' => 'http://buildwithreact.com/', 'user_id' => 3, 'topic_id' => 2, 'created_at' => '2015-10-04 13:02:34'],

            // NodeJS
            ['vote_count' => 33, 'title' => 'Felix\'s Node.js Guide', 'url' => 'http://nodeguide.com/', 'user_id' => 4, 'topic_id' => 3, 'created_at' => '2015-05-11 13:02:34'],
            ['vote_count' => 21, 'title' => 'NodeSchool.io', 'url' => 'http://nodeschool.io/', 'user_id' => 1, 'topic_id' => 3, 'created_at' => '2015-08-05 09:02:34'],
            ['vote_count' => 19, 'title' => 'CodeSchool', 'url' => 'https://www.codeschool.com/courses/real-time-web-with-node-js', 'user_id' => 4, 'topic_id' => 3, 'created_at' => '2015-04-03 13:02:34'],
            ['vote_count' => 4, 'title' => 'Tutorials Point', 'url' => 'http://www.tutorialspoint.com/nodejs/', 'user_id' => 2, 'topic_id' => 3, 'created_at' => '2015-03-04 13:02:34'],

            // Bootstrap
            ['vote_count' => 26, 'title' => 'Bootstrap Official Guide', 'url' => 'http://getbootstrap.com/css/', 'user_id' => 2, 'topic_id' => 4, 'created_at' => '2015-02-04 13:02:34'],
            ['vote_count' => 23, 'title' => 'Code School', 'url' => 'https://www.codeschool.com/courses/blasting-off-with-bootstrap', 'user_id' => 1, 'topic_id' => 4, 'created_at' => '2015-08-02 13:02:34'],
            ['vote_count' => 15, 'title' => 'w3schools', 'url' => 'http://www.w3schools.com/bootstrap/', 'user_id' => 1, 'topic_id' => 4, 'created_at' => '2016-01-05 13:02:34'],
            ['vote_count' => 10, 'title' => 'Tutorial Rebulic', 'url' => 'http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/', 'user_id' => 3, 'topic_id' => 4, 'created_at' => '2015-10-20 13:02:34'],
            ['vote_count' => 5, 'title' => 'Tutorials Point', 'url' => 'http://www.tutorialspoint.com/bootstrap/', 'user_id' => 4, 'topic_id' => 4, 'created_at' => '2016-01-06 13:02:34'],

            // MongoDB
            ['vote_count' => 25, 'title' => 'MongdoDB Manual', 'url' => 'https://docs.mongodb.org/manual/contents/', 'user_id' => 3, 'topic_id' => 5, 'created_at' => '2015-04-07 08:02:34'],
            ['vote_count' => 18, 'title' => 'Code School', 'url' => 'https://www.codeschool.com/courses/the-magical-marvels-of-mongodb', 'user_id' => 1, 'topic_id' => 5, 'created_at' => '2015-08-04 08:02:34'],
            ['vote_count' => 12, 'title' => 'w3resource', 'url' => 'http://www.w3resource.com/mongodb/introduction-mongodb.php', 'user_id' => 2, 'topic_id' => 5, 'created_at' => '2015-10-02 13:02:34'],
            ['vote_count' => 6, 'title' => 'Tutorials Point', 'url' => 'http://www.tutorialspoint.com/mongodb/', 'user_id' => 4, 'topic_id' => 5, 'created_at' => '2015-11-01 13:02:34'],

        ]);
    }
}
