<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('user')->insert([
            ['email' => 'lcgomez@student.apc.edu.ph', 'password' => 'salome', 'first_name' => 'Lee', 'last_name' => 'Gomez', 'confirmed' => 1],
            ['email' => 'paulg@student.apc.edu.ph', 'password' => 'salome', 'first_name' => 'Paul', 'last_name' => 'Graham', 'confirmed' => 1],
            ['email' => 'saltman@student.apc.edu.ph', 'password' => 'salome', 'first_name' => 'Sam', 'last_name' => 'Altman', 'confirmed' => 1],
            ['email' => 'aeinstein@student.apc.edu.ph', 'password' => 'salome', 'first_name' => 'Albert', 'last_name' => 'Einstein', 'confirmed' => 1],
        ]);
    }
}
