<?php

use Illuminate\Database\Seeder;

class PinnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pinner')->insert([
            ['user_id' => 1, 'topic_id' => 1],
            ['user_id' => 1, 'topic_id' => 2],
            ['user_id' => 1, 'topic_id' => 3],
            ['user_id' => 1, 'topic_id' => 4],
            ['user_id' => 1, 'topic_id' => 5],
        ]);
    }
}
