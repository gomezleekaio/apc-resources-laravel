<?php

use Illuminate\Database\Seeder;

class TopicTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topic')->insert([
            ['name' => 'AngularJS', 'description' => 'AngularJS is a JavaScript framework.', 'view_count' => 120],
            ['name' => 'ReactJS', 'description' => 'React is a JavaScript library for building user interfaces.', 'view_count' => 87],
            ['name' => 'NodeJS', 'description' => 'Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.', 'view_count' => 32],
            ['name' => 'Bootstrap', 'description' => 'Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.', 'view_count' => 55],
            ['name' => 'MongoDB', 'description' => 'MongoDB is the next-generation database that lets you create applications never before possible.', 'view_count' => 23],
        ]);
    }
}
